<%--
  Created by IntelliJ IDEA.
  User: Hyeonproejct
  Date: 2019-11-30
  Time: 오후 2:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Hyeon Tetsting</title>
</head>
<body>
    <c:set var="currentTip" value = "<b></b>" scope ="page"/>

    <div>
        <b>Tip of the Day:</b><br><br>
        <c:out value="${pageScope.currentTip}" escapeXml="true"/>tag
    </div>
</body>
</html>
