package example.control;

import javax.servlet.http.HttpSession;
import java.io.IOException;

public class TalentMarketController extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        //1. Initialize for request proecessing
        request.setCharacterEncoding("utf-8");

        HttpSession session = request.getSession(); //세션 객체 생성

        //2. dispatch request to processing componet
        String pathInfo = request.getPathInfo();
        String viewName = "error.jsp";

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        doPost(request,response);
    }
}
